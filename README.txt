MohrCirclePlotter

Matlab App to visualize stress element and Mohr circles for a given state of stress. To install the app, use 'MohrCircle.mlappinstall' located in './App'.

The source code of the app can be accessed using './mohrcircle.mlapp'. You need to include './quiver_tri' on Matlab path to run the app for modification.

The app was generated using Matlab 2022. It is tested to work with Matlab 2021b and above.

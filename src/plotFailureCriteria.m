function plotFailureCriteria(ah,Sy,Sut,Suc,sigA,sigB,nL,showMSS,showDE,showBCM,showMM,showLoadline,showDEsurf,showMSSsurf)

cla(ah);

% plot yield surface
hold(ah,'on');
plot3(ah,[0,1.5*Sy],[0,0],[0,0],'k-','LineWidth',1.5);
plot3(ah,[0,0],[0,1.5*Sy],[0,0],'k-','LineWidth',1.5);
plot3(ah,[0,0],[0,0],[0,1.5*Sy],'k-','LineWidth',1.5);

xpts_MSS = [Sy,Sy,0,-Sy,-Sy,0,Sy]';
ypts_MSS = [0,Sy,Sy,0,-Sy,-Sy,0]';

if showMSS
    hMSS = plot(ah,xpts_MSS,ypts_MSS,'-');
    hMSS.Color = [0,128,0]/255;
    hMSS.LineWidth = 2;
    ah.View = [0 90];
end

if showMSSsurf
    % Plot 3D MSS planes surface
    ztop = 3.0*Sy;
    zbottom = -3.0*Sy;
    zpts_MSS = [ones(6,1)*ztop, ones(6,1)*zbottom];

    MSSvertices = [ [ztop+xpts_MSS(1:end-1);zbottom+xpts_MSS(1:end-1)], [ztop+ypts_MSS(1:end-1);zbottom+ypts_MSS(1:end-1)], zpts_MSS(:) ];
    F_MSS = ...
        [1, 2, 8, 7;...
        2, 3, 9, 8;...
        3, 4, 10, 9;...
        4, 5, 11, 10;...
        5, 6, 12, 11;...
        6, 1, 7, 12];
    hMSSplane = patch(ah,'Faces',F_MSS, 'Vertices', MSSvertices);
    hMSSplane.EdgeColor = 'k';
    hMSSplane.FaceColor = [135,206,250]/255;
    hMSSplane.FaceAlpha = 0.5;
    if ~showMSS && ~showDE
        ah.View = [135 35];
    end
end


if showDE
    f = @(x,y) x.^2 + y.^2 - x.*y - Sy.^2;
    hDE = fimplicit(ah,f,1.5*[-Sy Sy -Sy Sy]);
    hDE.LineWidth = 2;
    hDE.LineStyle = '-';
    hDE.Color = 'r';
    ah.View = [0 90];
end

if showDEsurf
    % Plot 3D surface
    f3D = @(x,y,z) sqrt(((x-y).^2 + (y-z).^2 + (z-x).^2)/2) - Sy;
    hDEsurf = fimplicit3(ah,f3D,3.0*[-Sy Sy -Sy Sy -Sy Sy]);
    hDEsurf.EdgeColor = 'none';
    hDEsurf.FaceColor = [255,215,0]/255;
    hDEsurf.FaceAlpha = 0.5;
    if ~showMSS && ~showDE
        ah.View = [135 35];
    end
end

if showDEsurf || showMSSsurf
    % plot xy plane
    XYvertices = [ [-1.5*Sy;1.5*Sy;1.5*Sy;-1.5*Sy], [-1.5*Sy;-1.5*Sy;1.5*Sy;1.5*Sy], [0;0;0;0] ];
    F_XY = ...
        [1,2,3,4];
    hMSSplane = patch(ah,'Faces',F_XY, 'Vertices', XYvertices);
    hMSSplane.EdgeColor = 'none';
    hMSSplane.FaceColor = [221,160,221]/255;
    hMSSplane.FaceAlpha = 0.75;
end

if showBCM
    xpts_BCM = [Sut,Sut,0,-Suc,-Suc,0,Sut];
    ypts_BCM = [0,Sut,Sut,0,-Suc,-Suc,0];
    hBCM = plot(ah,xpts_BCM,ypts_BCM,'b-');
    hBCM.LineWidth = 2;
end

if showMM
    xpts_MM = [Sut,Sut,-Sut,-Suc,-Suc,0,Sut];
    ypts_MM = [-Sut,Sut,Sut,0,-Suc,-Suc,-Sut];
    hMM = plot(ah,xpts_MM,ypts_MM,'r-');
    hMM.LineWidth = 2;
end

if showLoadline
% Plot Load point
hLP = plot(ah,sigA, sigB,'ko');
hLP.MarkerSize = 10;
hLP.MarkerFaceColor = 'r';
hLP.LineWidth = 2.0;

% Plot overload point
hLP = plot(ah,nL*sigA, nL*sigB,'ks');
hLP.MarkerSize = 8;
hLP.MarkerFaceColor = 'b';
hLP.LineWidth = 2.0;

% Plot load line
maxloadfactor = 1.25*Sy/(max(abs(sigA),abs(sigB)));
hLP = plot(ah,[0, maxloadfactor*sigA], [0, maxloadfactor*sigB],'k-');
hLP.LineWidth = 1.0;
end

units = 'MPa';
% ah.XAxisLocation = "origin";
% ah.YAxisLocation = "origin";

ah.XRuler.FirstCrossoverValue  = 0; % X crossover with Y axis
ah.XRuler.SecondCrossoverValue  = 0; % X crossover with Z axis
ah.YRuler.FirstCrossoverValue  = 0; % Y crossover with X axis
ah.YRuler.SecondCrossoverValue  = 0; % Y crossover with Z axis
ah.ZRuler.FirstCrossoverValue  = 0; % Z crossover with X axis
ah.ZRuler.SecondCrossoverValue = 0; % Z crossover with Y axis

% ah.XLabel.String = sprintf('s_A [%s]',units);
% ah.XLabel.FontSize = 12;
% ah.YLabel.String = sprintf('s_B [%s]',units);
% ah.YLabel.FontSize = 12;

ah.DataAspectRatio = [1 1 1];

hold(ah,'off');